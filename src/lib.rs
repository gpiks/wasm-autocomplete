use wasm_bindgen::prelude::*;
use wasm_bindgen::describe::*;
use wasm_bindgen::convert::*;
use trie_rs::{Trie, TrieBuilder};

pub trait IntoWasmAbi: WasmDescribe {
    type Abi: WasmAbi;
    fn into_abi(self, extra: &mut Trie<u8>) -> Self::Abi;
}

/// A Python class for a FST
#[wasm_bindgen]
pub struct TrieRs {
    #[wasm_bindgen(skip)]
    pub trie: Trie<u8>
}

#[wasm_bindgen]
pub fn count_strings_in_set(words: &js_sys::Array) -> TrieRs {
    let mut count = 0;
    use web_sys::console;

    let mut builder = TrieBuilder::new();

    for x in words.to_vec() {
        let inter = x.as_string();

        builder.push(inter.unwrap());

        if x.is_string() {
            count += 1;
        }
    }

    let js: JsValue = count.into();
    console::log_1(&js);
    TrieRs { trie: builder.build() }
}
