use wasm_bindgen_test::*;
use wasm_bindgen::JsValue;

#[wasm_bindgen_test]
fn pass() {
    let test_array: js_sys::Array = js_sys::Array::new_with_length(3);
    test_array.set(0, JsValue::from_str("hello"));
    test_array.set(1, JsValue::from_str("bello"));
    test_array.set(2, JsValue::from_str("mello"));
    count_strings_in_set(test_array);
}
